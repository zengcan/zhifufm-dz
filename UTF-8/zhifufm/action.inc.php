<?php

if (!defined('IN_DISCUZ')) {
    exit('Access Denied');
}

include_once DISCUZ_ROOT . './source/plugin/zhifufm/zhifufmcore.php';

$action = $_GET['do'];
$invalidActions = array('check', 'notify', 'return', 'payr', 'result', 'viewdb');
if (in_array($action, $invalidActions)) {
    $logger->debug('[ACT:' . $action . '] ----- action start -----');
    $zhifufm = new zhifufmCore();
    $methodName = 'api' . ucfirst(strtolower($action));
    $logger->debug('[ACT:' . $action . '] => zhifufm.' . $methodName);
    echo call_user_func(array($zhifufm, $methodName));
    $logger->debug('[ACT:' . $action . '] ----- action end -----');
} else {
    echo "Error Action: " . $action;
}
exit();

