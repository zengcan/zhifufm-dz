<?php
if (!defined('IN_DISCUZ')) {
	exit('Access Denied');
}

date_default_timezone_set('Asia/Shanghai');

define('ZHIFUFM_PAY_TYPE_WXN', "wechat");
define('ZHIFUFM_PAY_TYPE_ZFB', "alipay");
define('ZHIFUFM_WWW_DIR', realpath(__DIR__ . '/../../../'));
define('ZHIFUFM_LOG_DIR', ZHIFUFM_WWW_DIR . '/data/log');
define('ZHIFUFM_LOG_FILE', ZHIFUFM_LOG_DIR . '/zhifufm.log');
define('ZHIFUFM_DB_FILE', ZHIFUFM_WWW_DIR . '/data/zhifufmdb.sqlite3');

// define('ZHIFUFM_API_USER', '216456dd');
// define('ZHIFUFM_API_KEY', '659fdded-c762-480f-a83b-5c2a11a37bc6');

####################################################################################

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

function FriendlyErrorType($type)
{
	switch ($type) {
		case E_ERROR: // 1 //
			return 'E_ERROR';
		case E_WARNING: // 2 //
			return 'E_WARNING';
		case E_PARSE: // 4 //
			return 'E_PARSE';
		case E_NOTICE: // 8 //
			return 'E_NOTICE';
		case E_CORE_ERROR: // 16 //
			return 'E_CORE_ERROR';
		case E_CORE_WARNING: // 32 //
			return 'E_CORE_WARNING';
		case E_COMPILE_ERROR: // 64 //
			return 'E_COMPILE_ERROR';
		case E_COMPILE_WARNING: // 128 //
			return 'E_COMPILE_WARNING';
		case E_USER_ERROR: // 256 //
			return 'E_USER_ERROR';
		case E_USER_WARNING: // 512 //
			return 'E_USER_WARNING';
		case E_USER_NOTICE: // 1024 //
			return 'E_USER_NOTICE';
		case E_STRICT: // 2048 //
			return 'E_STRICT';
		case E_RECOVERABLE_ERROR: // 4096 //
			return 'E_RECOVERABLE_ERROR';
		case E_DEPRECATED: // 8192 //
			return 'E_DEPRECATED';
		case E_USER_DEPRECATED: // 16384 //
			return 'E_USER_DEPRECATED';
	}
	return "";
}

function log_trace($message = '')
{
	global $logger;
	
	$trace = debug_backtrace();
	if ($message) {
		$logger->error($message);
	}
	$trace = array_reverse($trace);
	$caller = array_shift($trace);
	
	$_function = isset($caller['function']) ? $caller['function'] : '(func)';
	$_file = isset($caller['file']) ? $caller['file'] : '(file)';
	$_line = isset($caller['line']) ? $caller['line'] : '0';
	
	$logger->error(sprintf('%s: Called from %s:%s', $_function, $_file, $_line));
	foreach ($trace as $entry_id => $entry) {
		$_class = isset($entry['class']) ? $entry['class'] : '';
		$_function = isset($entry['function']) ? $entry['function'] : '(func)';
		$_file = isset($entry['file']) ? $entry['file'] : '(file)';
		$_line = isset($entry['line']) ? $entry['line'] : '0';
		if (empty($_class)) {
			$logger->error(sprintf('%3s. %s() %s:%s', $entry_id + 1, $_function, $_file, $_line));
		} else {
			$logger->error(sprintf('%3s. %s->%s() %s:%s', $entry_id + 1, $_class, $_function, $_file, $_line));
		}
	}
}

function handleError($code, $message, $file, $line)
{
	global $logger;
	$level = in_array($code, array(E_DEPRECATED, E_USER_DEPRECATED, E_NOTICE)) ? 'warn' : 'error';
	$title = FriendlyErrorType($code);
	$title = $title ? $title : 'Error #' . $code;
	call_user_func(array($logger, $level), $title . ': ' . $message . ' on ' . $file . ' line ' . $line);
	if ($level === 'error') {
		log_trace();
	}
}

function handleException($code, $message="", $file, $line)
{
	global $logger;
	$logger->error('Exception ' . $code . ': ' . $message . ' On ' . $file . ' line ' . $line);
	log_trace();
}

function handleShutdown()
{
	global $logger;
	$logger->info('Shutdown');
}

####################################################################################

require_once DISCUZ_ROOT . './source/plugin/zhifufm/zhifufmlogger.php';

global $logger;
$logger = new zhifufmLogger(ZHIFUFM_LOG_FILE, DEBUG);

set_error_handler('handleError');
set_exception_handler('handleException');
register_shutdown_function('handleShutdown');

require_once DISCUZ_ROOT . './source/plugin/zhifufm/zhifufmdb.php';

global $_db;
$_db = new zhifufmDB();

####################################################################################

class zhifufmCore
{
	public function apiInstall()
	{
	}
	
	public function apiUninstall()
	{
		die('<h1>Sorry!</h1><desc style="font-size:x-large">暂不支持卸载操作...</desc>');
	}
	
	public function apiTest()
	{
		die('<div style="font-size:10em;font-weight:bold;width:100%;text-align:center;">^_^</div>');
	}
	
	public static function getPayForm($orderAmount, &$orderID, $payType, $orderInfo = null)
	{
		$params = self::getParams($orderAmount, $orderID, $payType, $orderInfo);
		$apiurl = $params['api_url'];
		$merchantNum = $params['api_user'];
		$amount = $params['price'];
		$payType = $params['type'];
		$returnUrl = $params['redirect'];
		$notifyUrl = $params['notify'];
		$subject = $params['order_info'];
		$orderNo = $orderID;
		$returnType = "page";
		$sign = $params['signature'];
		
		$html = '<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>redirect……</title></head>
<body>
<form style="display:none" id="fPay" method="post" action="$apiurl">
    <input name="merchantNum" type="text" value="$merchantNum"/>
    <input name="amount" type="text" value="$amount"/>
    <input name="payType" type="text" value="$payType"/>
    <input name="returnUrl" type="text" value="$returnUrl"/>
	<input name="notifyUrl" type="text" value="$notifyUrl"/>
    <input name="orderNo" type="text" value="$orderID"/>
    <input name="subject" type="text" value="$subject"/>
	<input type="hidden" name="returnType" value="$returnType"/>
    <input name="sign" type="text" value="$sign"/>
				
</form>
<script type="text/javascript">
    window.onload = function () {
        document.getElementById("fPay").submit();
    };
</script></body></html>';
		if (preg_match_all('/\$([a-zA-Z]+)/', $html, $matches)) {
			foreach ($matches[1] as $index => $varName) {
				$html = str_replace($matches[0][$index], sprintf('%s', $$varName), $html);
			}
		}
		return $html;
	}
	
	public static function getParams($orderAmount, &$orderID, $payType, $orderInfo = null)
	{
		global $_G, $_db, $config;
		if (!$orderID) {
			$orderID = dgmdate(TIMESTAMP, 'YmdHis') . random(18);
		}
		if (!$orderInfo) {
			$orderInfo = 'DZ订单:' . $orderID;
		}
		
		// $apiUser = ZHIFUFM_API_USER;
		// $apiKey = ZHIFUFM_API_KEY;
		// zhifufmOrder::apiConfig($apiUser, $apiKey);
		
		$redirectURL = $_G['siteurl'] . 'plugin.php?id=zhifufm:action&do=return&pk=' . base64_encode($orderID);
		$notifyURL =  $_G['siteurl'] . 'plugin.php?id=zhifufm:notify';
		$order = new zhifufmOrder($orderID, $orderAmount, $orderInfo, $payType, $notifyURL);
		$signature = $order->signature();
		
		$reason = 'credit';  # 暂时只测试积分
		$extra = '';
		$_db->insert($reason, $config['merchantNum'], $orderAmount, $payType, $redirectURL, $orderID, $orderInfo, $signature, $extra);
		
		return array(
				'api_url' => $config['api_url'],
				'api_user' => $config['merchantNum'],
				'price' => $orderAmount,
				'type' => $payType,
				'redirect' => $redirectURL,
				'notify' => $notifyURL,
				'order_id' => $orderID,
				'order_info' => $orderInfo,
				'signature' => $signature,
		);
	}
	
	public static function getPayURL($orderAmount, &$orderID, $payType, $orderInfo = null)
	{
		$params = self::getParams($orderAmount, $orderID, $payType, $orderInfo);
		$url = 'https://zfapi.nnt.ltd/api/startOrder?';
		foreach ($params as $key => $val) {
			$url .= '&' . $key . '=' . urlencode($val);
		}
		return $url;
	}
	
	function getRecord($orderID)
	{
		$order = C::t('forum_order')->fetch($orderID);
		return $order;
	}
	
	function apiCheck()
	{
		global $_db;
		$orderID = isset($_GET['pk']) ? trim(base64_decode($_GET['pk'])) : '';
		if ($orderID) {
			// $row = $this->getRecord($orderID);
			// if ($row['status'] === '2' || $row['status'] === '3') {
			$row = $_db->getByOrderID($orderID);
			if ($row && $row['result'] == 'success') {
				return 'success';
			}
			} else {
				header('HTTP/1.1 404 Not Found');
			}
			header('Content-Type: text/plain');
			return 'error';
		}
		
		public function apiReturn()
		{
			global $_G, $_db, $logger;
			
			header("Content-Type: text/html;charset=utf-8");
			
			$logger->info('[' . $_G['clientip'] . '] zhifufm has returned, GET: ' . var_export($_GET, true));
			try {
				$orderID = trim(base64_decode($_GET['pk']));
				$row = $this->getRecord($orderID);
			} catch (Exception $error) {
				$logger->warn($error);
				header('HTTP/1.1 404 Not Found');
				return '<h1>Not Found</h1>';
			}
			$_db->afterReturn($orderID);
			
			# wait.html
			$orderID = base64_encode($orderID);
			$html = base64_decode('PCFET0NUWVBFIGh0bWw+CjxodG1sIGxhbmc9InpoLWNuIj4KPGhlYWQ+Cgk8bWV0YSBjaGFyc2V0PSJ1dGYtOCIvPgoJPG1ldGEgaHR0cC1lcXVpdj0iY29udGVudC10eXBlIiBjb250ZW50PSJ0ZXh0L2h0bWw7Y2hhcnNldD1VVEYtOCI+Cgk8bWV0YSBuYW1lPSJ2aWV3cG9ydCIgY29udGVudD0id2lkdGg9ZGV2aWNlLXdpZHRoLGluaXRpYWwtc2NhbGU9MSIvPgoJPHRpdGxlPuaUr+S7mOeKtuaAgeehruiupOS4rS4uLjwvdGl0bGU+Cgk8c3R5bGUgdHlwZT0idGV4dC9jc3MiPgoJCWJvZHkgewoJCQl0ZXh0LWFsaWduOiBjZW50ZXI7CgkJfQoKCQkud3JhcCB7CgkJCXdpZHRoOiA2NHB4OwoJCQloZWlnaHQ6IDY0cHg7CgkJCXBvc2l0aW9uOiByZWxhdGl2ZTsKCQkJbWFyZ2luOiAxMDBweCBhdXRvIDA7CgkJfQoKCQkub3V0ZXIgewoJCQlwb3NpdGlvbjogYWJzb2x1dGU7CgkJCXdpZHRoOiAxMDAlOwoJCQloZWlnaHQ6IDEwMCU7CgkJCWFuaW1hdGlvbjogc3BpbiA4MDBtcyBpbmZpbml0ZSBsaW5lYXI7CgkJfQoKCQkuaW5uZXIsIC5pbm5lcjIgewoJCQlwb3NpdGlvbjogYWJzb2x1dGU7CgkJCXdpZHRoOiAzOHB4OwoJCQloZWlnaHQ6IDM4cHg7CgkJCWJvcmRlci1yYWRpdXM6IDQwcHg7CgkJCW92ZXJmbG93OiBoaWRkZW47CgkJCWxlZnQ6IDEzcHg7CgkJCXRvcDogMTNweDsKCQl9CgoJCS5pbm5lciB7CgkJCW9wYWNpdHk6IDE7CgkJCWJhY2tncm91bmQtY29sb3I6ICM4OWFiZGQ7CgkJCWFuaW1hdGlvbjogc2Vjb25kLWhhbGYtaGlkZSAxLjZzIHN0ZXBzKDEsIGVuZCkgaW5maW5pdGU7CgkJfQoKCQkuaW5uZXIyIHsKCQkJb3BhY2l0eTogMDsKCQkJYmFja2dyb3VuZC1jb2xvcjogIzRiODZkYjsKCQkJYW5pbWF0aW9uOiBzZWNvbmQtaGFsZi1zaG93IDEuNnMgc3RlcHMoMSwgZW5kKSBpbmZpbml0ZTsKCQl9CgoJCS5zcGluZXIsIC5maWxsZXIsIC5tYXNrZXIgewoJCQlwb3NpdGlvbjogYWJzb2x1dGU7CgkJCXdpZHRoOiA1MCU7CgkJCWhlaWdodDogMTAwJTsKCQl9CgoJCS5zcGluZXIgewoJCQlib3JkZXItcmFkaXVzOiA0MHB4IDAgMCA0MHB4OwoJCQliYWNrZ3JvdW5kLWNvbG9yOiAjNGI4NmRiOwoJCQl0cmFuc2Zvcm0tb3JpZ2luOiByaWdodCBjZW50ZXI7CgkJCWFuaW1hdGlvbjogc3BpbiA4MDBtcyBpbmZpbml0ZSBsaW5lYXI7CgkJCWxlZnQ6IDA7CgkJCXRvcDogMDsKCQl9CgoJCS5maWxsZXIgewoJCQlib3JkZXItcmFkaXVzOiAwIDQwcHggNDBweCAwOwoJCQliYWNrZ3JvdW5kLWNvbG9yOiAjNGI4NmRiOwoJCQlhbmltYXRpb246IHNlY29uZC1oYWxmLWhpZGUgODAwbXMgc3RlcHMoMSwgZW5kKSBpbmZpbml0ZTsKCQkJbGVmdDogNTAlOwoJCQl0b3A6IDA7CgkJCW9wYWNpdHk6IDE7CgkJfQoKCQkubWFza2VyIHsKCQkJYm9yZGVyLXJhZGl1czogNDBweCAwIDAgNDBweDsKCQkJYmFja2dyb3VuZC1jb2xvcjogIzg5YWJkZDsKCQkJYW5pbWF0aW9uOiBzZWNvbmQtaGFsZi1zaG93IDgwMG1zIHN0ZXBzKDEsIGVuZCkgaW5maW5pdGU7CgkJCWxlZnQ6IDA7CgkJCXRvcDogMDsKCQkJb3BhY2l0eTogMDsKCQl9CgoJCS5pbm5lcjIgLnNwaW5lciwgLmlubmVyMiAuZmlsbGVyIHsKCQkJYmFja2dyb3VuZC1jb2xvcjogIzg5YWJkZDsKCQl9CgoJCS5pbm5lcjIgLm1hc2tlciB7CgkJCWJhY2tncm91bmQtY29sb3I6ICM0Yjg2ZGI7CgkJfQoKCQlAa2V5ZnJhbWVzIHNwaW4gewoJCQkwJSB7CgkJCQl0cmFuc2Zvcm06IHJvdGF0ZSgzNjBkZWcpOwoJCQl9CgkJCTEwMCUgewoJCQkJdHJhbnNmb3JtOiByb3RhdGUoMGRlZyk7CgkJCX0KCQl9CgoJCUBrZXlmcmFtZXMgc2Vjb25kLWhhbGYtaGlkZSB7CgkJCTAlIHsKCQkJCW9wYWNpdHk6IDE7CgkJCX0KCQkJNTAlLCAxMDAlIHsKCQkJCW9wYWNpdHk6IDA7CgkJCX0KCQl9CgoJCUBrZXlmcmFtZXMgc2Vjb25kLWhhbGYtc2hvdyB7CgkJCTAlIHsKCQkJCW9wYWNpdHk6IDA7CgkJCX0KCQkJNTAlLCAxMDAlIHsKCQkJCW9wYWNpdHk6IDE7CgkJCX0KCQl9Cgk8L3N0eWxlPgo8L2hlYWQ+Cjxib2R5Pgo8ZGl2IGNsYXNzPSJ3cmFwIj4KCTxkaXYgY2xhc3M9Im91dGVyIj48L2Rpdj4KCTxkaXYgY2xhc3M9ImlubmVyIj4KCQk8ZGl2IGNsYXNzPSJzcGluZXIiPjwvZGl2PgoJCTxkaXYgY2xhc3M9ImZpbGxlciI+PC9kaXY+CgkJPGRpdiBjbGFzcz0ibWFza2VyIj48L2Rpdj4KCTwvZGl2PgoJPGRpdiBjbGFzcz0iaW5uZXIyIj4KCQk8ZGl2IGNsYXNzPSJzcGluZXIiPjwvZGl2PgoJCTxkaXYgY2xhc3M9ImZpbGxlciI+PC9kaXY+CgkJPGRpdiBjbGFzcz0ibWFza2VyIj48L2Rpdj4KCTwvZGl2Pgo8L2Rpdj4K5pSv5LuY54q25oCB56Gu6K6k5LitCjxzY3JpcHQ+Cgl2YXIgY2hlY2tVUkwgPSAnL3BsdWdpbi5waHA/aWQ9cHB6OmFjdGlvbiZkbz1jaGVjayZwaz0kb3JkZXJJRCc7CgoJZnVuY3Rpb24gcHB6Q2hlY2soKSB7CgkJdmFyIHhociA9IG51bGw7CgkJaWYgKHdpbmRvdy5YTUxIdHRwUmVxdWVzdCkgewoJCQl4aHIgPSBuZXcgWE1MSHR0cFJlcXVlc3QoKTsKCQl9IGVsc2UgewoJCQl4aHIgPSBuZXcgQWN0aXZlWE9iamVjdCgnTWljcm9zb2Z0LlhNTEhUVFAnKTsKCQl9CgkJeGhyLm9wZW4oJ0dFVCcsIGNoZWNrVVJMLCB0cnVlKTsKCQl4aHIuc2VuZCgpOwoJCXhoci5vbnJlYWR5c3RhdGVjaGFuZ2UgPSBmdW5jdGlvbiAoKSB7CgkJCWNvbnNvbGUubG9nKHhocik7CgkJCWlmICh4aHIucmVhZHlTdGF0ZSA9PT0gNCkgewoJCQkJaWYgKHhoci5zdGF0dXMgPT09IDIwMCkgewoJCQkJCXZhciBjb250ZW50ID0geGhyLnJlc3BvbnNlVGV4dDsKCQkJCQljb25zb2xlLmxvZyhjb250ZW50KTsKCQkJCQlpZiAoY29udGVudCAhPT0gJ2Vycm9yJykgewoJCQkJCQlsb2NhdGlvbi5ocmVmID0gJy9mb3J1bS5waHA/bW9kPW1pc2MmYWN0aW9uPXBheXN1Y2NlZWQnOwoJCQkJCQlyZXR1cm47CgkJCQkJfQoJCQkJCXNldFRpbWVvdXQocHB6Q2hlY2ssIDEwMDApOwoJCQkJfQoJCQl9CgkJfTsKCX0KCglzZXRUaW1lb3V0KHBwekNoZWNrLCAxMDAwKTsKPC9zY3JpcHQ+CjwvYm9keT4KPC9odG1sPg');
			if (preg_match_all('/\$([a-zA-Z]+)/', $html, $matches)) {
				foreach ($matches[1] as $index => $varName) {
					$html = str_replace($matches[0][$index], sprintf('%s', $$varName), $html);
				}
			}
			return $html;
		}
		
		protected function _apiNotify()
		{
			//     	GET: array (
			//     			'id' => 'zhifufm:notify',
			//     			'amount' => '0.1',
			//     			'orderNo' => '202009245f6c47dd33758',
			//     			'actualPayAmount' => '0.1',
			//     			'payTime' => '2020-09-24 15:18:17',
			//     			'platformOrderNo' => '1309029178980433920',
			//     			'merchantNum' => 'tencent',
			//     			'sign' => '6358b2adeadbe2cf01442a0f9b45ae5e',
			//     			'state' => '1',
			//     	)
			
			global $_db;
			
			try {
				$orderID = $_GET['orderNo'];
				$orderAmount = $_GET['amount'];
				$orderAmountReal = $_GET['actualPayAmount'];
				$zhifufmOrderID = $_GET['platformOrderNo'];
				$signature = $_GET['sign'];
				$state = $_GET['state'];
				$merchantNum = $_GET['merchantNum'];
				$newSignature = zhifufmOrder::verifySignature(array(
						"order_id" => $orderID,
						"price" => $orderAmount,
						"state" => $state,
						"merchantNum"=> $merchantNum
				));
				if ($newSignature !== $signature) {
					return 'signature error: ' . $newSignature . ' != ' . $signature;
				}
				
				$_db->afterNotify($orderID, $zhifufmOrderID, $orderAmountReal, $signature);
				return 'success';
			} catch (Exception $error) {
				return 'Error';
			}
		}
		
		public function apiNotify()
		{
			global $_G, $_db, $logger;
			
			$logger->info('[' . $_G['clientip'] . '] [NOTIFY]  GET: ' . var_export($_GET, true));
			//$logger->info('[' . $_G['clientip'] . '] [NOTIFY] POST: ' . var_export($_POST, true));
			
			try {
				$orderID = $_GET['orderNo'];
				$msg = $this->_apiNotify();
				$_db->storeResult($orderID, $msg);
			} catch (Exception $error) {
				$logger->error($error->getMessage() . "\n" . $error->getTraceAsString());
				header("HTTP/1.1 500 Internal Server Error");
				$msg = 'Error';
			}
			
			$logger->info('[' . $_G['clientip'] . '] [NOTIFY] Result: ' . $msg);
			return $msg;
		}
		
		public function apiViewdb()
		{
			if(!defined('IN_DISCUZ')) {
				exit('Access Denied');
			}
			header("Content-Type: text/html;charset=utf-8");
			$html = <<<'EOF'
<!DOCTYPE html>
<html lang="zh-CN">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>zhifufmDB Viewer</title>
	<link href="https://cdn.bootcss.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">
	<!--[if lt IE 9]>
	<script src="https://cdn.bootcss.com/html5shiv/3.7.3/html5shiv.min.js"></script>
	<script src="https://cdn.bootcss.com/respond.js/1.4.2/respond.min.js"></script>
	<![endif]-->
</head>
<body>
<div class="container-fluid">
<h1>zhifufmDB Viewer</h1>
<pre>
EOF;
			$html .= zhifufmDB::$createSQL;
			$html .= <<<'EOF'
</pre>
<table class="table table-striped">
EOF;
			$db = new zhifufmDB();
			$rows = $db->getAll(true);
			foreach ($rows as $row) {
				$result = isset($row['result']) && $row['result'] ? $row['result'] : '[[Null]]';
				// $result = htmlentities($result);
				$result = htmlspecialchars($result, ENT_QUOTES, "UTF-8");
				$extra = var_export(json_decode($row['extra'], true), true);
				$html .= <<<EOF
	<tr>
		<td rowspan="3"><h3>{$row['id']}</h3></td>
		<td>
			<table class="table table-striped">
				<tr>
					<td>reason</td>
					<td>apiUser</td>
					<td>price</td>
					<td>type</td>
					<td>redirect</td>
					<td>orderID</td>
					<td>orderInfo</td>
					<td>signature</td>
					<td>created_at</td>
				</tr>
				<tr>
					<td>{$row['reason']}</td>
					<td>{$row['api_user']}</td>
					<td>{$row['price']}</td>
					<td>{$row['type']}</td>
					<td>{$row['redirect']}</td>
					<td>{$row['order_id']}</td>
					<td>{$row['order_info']}</td>
					<td>{$row['signature']}</td>
					<td>{$row['created_at']}</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr><td>{$extra}</td></tr>
	<tr>
		<td>
			<table class="table table-striped">
				<tr>
					<td>zhifufm_order_id</td>
					<td>zhifufm_price</td>
					<td>zhifufm_signature</td>
					<td>returned_at</td>
					<td>notified_at</td>
					<td>notify_sate</td>
				</tr>
				<tr>
					<td>{$row['zhifufm_order_id']}</td>
					<td>{$row['zhifufm_price']}</td>
					<td>{$row['zhifufm_signature']}</td>
					<td>{$row['returned_at']}</td>
					<td>{$row['notified_at']}</td>
<td>{$row['result']}</td>
				</tr>
			</table>
		</td>
	</tr>
	
EOF;
			}
			$html .= <<<'EOF'
</table>
</div>
<script src="https://cdn.bootcss.com/jquery/1.12.4/jquery.min.js"></script>
<script src="https://cdn.bootcss.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</body>
</html>
EOF;
			die($html);
		}
	}
	
	?>
