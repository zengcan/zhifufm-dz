<?php
if (!defined('IN_DISCUZ')) {
    exit('Access Denied');
}

require_once DISCUZ_ROOT . './source/plugin/zhifufm/zhifufmlogger.php';

class zhifufmOrder
{
    private $signatureCache = NULL;
    private static $apiUser = NULL;
    private static $apiKey = NULL;

    public function __construct($orderID, $orderAmount, $orderInfo, $payType, $redirectURL)
    {
        $this->orderID = $orderID;
        $this->orderAmount = $orderAmount;
        $this->orderInfo = $orderInfo;
        $this->payType = $payType;
        $this->redirectURL = $redirectURL; //异步回调地址

        $params = array(
            $this->orderID,
            $this->orderInfo,
            $this->orderAmount,
            $this->redirectURL,
            $this->payType
        );
        $this->logInfo('create order: ' . implode('|', $params));
    }

    public function logInfo($message)
    {
        global $logger;
        $logger->info('[' . $this->orderID . '] ' . $message);
    }

    public static function apiConfig($apiUser, $apiKey)
    {
        self::$apiUser = $apiUser;
        self::$apiKey = $apiKey;
    }

    public static function verifySignature($respArray)
    {
        global $logger;
        //$respStr = implode('', array_values($respArray));
        //md5(付款成功状态state的值+商户号merchantNum的值+商户订单号orderNo的值+订单金额amount的值+商户秘钥)
        $signature = md5($respArray['state'].$respArray['merchantNum'].$respArray['order_id'].$respArray['price'].self::$apiKey);
        $logger->debug('verify response signature: ' . $respArray['state'].$respArray['merchantNum'].$respArray['order_id'].$respArray['price'].self::$apiKey . ' => ' . $signature);
        return $signature;
    }

    //md5(商户号+商户订单号+支付金额+异步通知地址+商户秘钥)
    public function signature()
    {
        if (is_null($this->signatureCache)) {
//             $params = array(
//                 $this::$apiKey, $this::$apiUser,
//                 $this->orderID, $this->orderInfo, $this->orderAmount, $this->redirectURL, $this->payType
//             );
            $this->signatureCache = md5($this::$apiUser.$this->orderID.$this->orderAmount.$this->redirectURL.$this::$apiKey);
            $this->logInfo('sign: ' . $this::$apiUser.$this->orderID.$this->orderAmount.$this->redirectURL.$this::$apiKey. ' => ' . $this->signatureCache);
        }
        return $this->signatureCache;
    }

    public function save()
    {
    }

    public function asData()
    {
        return array(
            'apiUser' => $this::$apiUser,
            'payType' => $this->payType,
            'orderID' => $this->orderID,
            'orderAmount' => $this->orderAmount,
            'orderInfo' => $this->orderInfo,
            'redirectURL' => $this->redirectURL,
            'signature' => $this->signature(),
        );
    }
}
