<?php
if (!defined('IN_DISCUZ')) {
    exit('Access Denied');
}

function zhifufmlog($message, $label = '-')
{
    global $logger;

    $message = "[$label] $message";
    // echo date('Y-m-d H:i:s') . ' ' . $message . PHP_EOL;
    $logger->debug($message);
}

class zhifufmDB
{
    static $createSQL = <<<'EOF'
CREATE TABLE `zhifufm_orders` (
  `id` INTEGER PRIMARY KEY AUTOINCREMENT,

  `reason` VARCHAR(50) NOT NULL,

  `api_user` VARCHAR(50) NOT NULL,
  `price` REAL NOT NULL,
  `type` VARCHAR(50) NOT NULL,
  `redirect` VARCHAR(100),
  `order_id` VARCHAR(128) NOT NULL,
  `order_info` VARCHAR(128) NOT NULL,
  `signature` VARCHAR(100) NOT NULL,

  `zhifufm_order_id` VARCHAR(50),
  `zhifufm_price` VARCHAR(50),
  `zhifufm_signature` VARCHAR(50),

  `created_at` VARCHAR(50) NOT NULL,
  `returned_at` VARCHAR(50),
  `notified_at` VARCHAR(50),

  `extra` TEXT,
  `result` TEXT
);
EOF;

    function __construct($dbFile = null, $force = false)
    {
        $dbFile = $dbFile ?: ZHIFUFM_DB_FILE;
        $needInitial = $force || !is_file($dbFile);
        if ($needInitial) {
            zhifufmlog('zhifufmdb init: ' . $dbFile);


            $fp = fopen($dbFile, 'w');
            fclose($fp);
            $this->db = new SQLite3($dbFile);
            $this->db->exec(self::$createSQL);
        } else {
            $this->db = new SQLite3($dbFile);
        }
    }


    function exec($sql)
    {
        zhifufmlog($sql, 'SQL');
        return $this->db->exec($sql);
    }

    function query($sql)
    {
        zhifufmlog($sql, 'SQL');
        return $this->db->query($sql);
    }

    function insert($reason, $apiUser, $price, $type, $redirect, $orderID, $orderInfo, $signature, $extra = '')
    {
        $reason = addslashes($reason);
        $a = addslashes($apiUser);
        $r = addslashes($redirect);
        $o = addslashes($orderID);
        $oi = addslashes($orderInfo);
        $s = addslashes($signature);
        $c = date('Y-m-d H:i:s');
        $sql = "INSERT INTO `zhifufm_orders` (`reason`, `api_user`, `price`, `type`, `redirect`, `order_id`, `order_info`, `signature`, `extra`, `created_at`) VALUES ('$reason', '$a', $price, '$type', '$r', '$o', '$oi', '$s', '$extra', '$c');";
        $this->exec($sql);
    }

    function afterReturn($orderID)
    {
        $row = $this->getByOrderID($orderID);
        if ($row) {
            if ($row['returned_at']) {
                zhifufmlog("The order $orderID returned once ({$row['returned_at']})!", 'WARN');
            }
            $currentTime = date('Y-m-d H:i:s');
            $sql = "UPDATE `zhifufm_orders` SET `returned_at` = '$currentTime' WHERE `id` = '{$row['id']}';";
            $this->exec($sql);
        }
    }

    function afterNotify($orderID, $zhifufmOrderID, $zhifufmPrice, $zhifufmSignature)
    {
        $row = $this->getByOrderID($orderID);
        if ($row) {
            if ($row['notified_at']) {
                zhifufmlog("The order $orderID notified once ({$row['notified_at']})!", 'WARN');
            }
            $o = addslashes($zhifufmOrderID);
            $s = addslashes($zhifufmSignature);
            $currentTime = date('Y-m-d H:i:s');
            $sql = "UPDATE `zhifufm_orders` SET `zhifufm_order_id` = '$o', `zhifufm_price` = $zhifufmPrice, `zhifufm_signature` = '$s', `notified_at` = '$currentTime' WHERE `id` = '{$row['id']}';";
            $this->exec($sql);
        }
    }

    function storeResult($orderID, $result)
    {
        $row = $this->getByOrderID($orderID);
        if ($row) {
            if ($row['result']) {
                zhifufmlog("The order $orderID has result ({$row['result']})!", 'WARN');
            }
            $result = addslashes($result);
            $sql = "UPDATE `zhifufm_orders` SET `result` = '$result' WHERE `id` = '{$row['id']}';";
            $this->exec($sql);
        }
    }

    function fetch($sql)
    {
        $results = $this->query($sql);
        $data = array();
        while ($res = $results->fetchArray(SQLITE3_ASSOC)) {
            array_push($data, $res);
        }
        return $data;
    }

    function getByOrderID($orderID)
    {
        $sql = 'SELECT * FROM `zhifufm_orders` WHERE `order_id` = "' . addslashes($orderID) . '" ORDER BY `id` DESC LIMIT 1;';
        $data = $this->fetch($sql);
        return $data ? $data[0] : null;
    }

    function getAll($reversed = false)
    {
        $sql = 'SELECT * FROM `zhifufm_orders`' . ($reversed ? ' ORDER BY `id` DESC' : '') . ';';
        return $this->fetch($sql);
    }
}
