<?php
if (!defined('IN_DISCUZ')) {
    exit('Access Denied');
}
include_once DISCUZ_ROOT . './source/plugin/zhifufm/main.inc.php';
include_once DISCUZ_ROOT . './source/plugin/zhifufm/zhifufmcore.php';
if (!$_G['uid']) showmessage('not login');
$type = $_POST['type'];
$money = isset($_POST['money']) ? (float)$_POST['money'] : 0.0;
$need = (int)$_POST['addfundamount'];

if ($need < (int)$config['zhifufm_mincredits'] || ($need > 0 && $config['zhifufm_maxcredits'] && $need > $config['zhifufm_maxcredits'])) {
    showmessage('zhifufm:amount_invalid', '', array('zhifufm_maxcredits' => $config['zhifufm_maxcredits'], 'zhifufm_mincredits' => $config['zhifufm_mincredits']));
}
$pay_bank = unserialize($config['payType']);
foreach ($pay_bank as $key => $value) {
	if ($value == 'wechat') {
		$pay_banks['wechat'] = "wechat";
	} else if ($value == 'alipay') {
		$pay_banks['alipay'] = 'alipay';
	}else if ($value == 'alipaysign') {
		$pay_banks['alipaysign'] = 'alipaysign';
	}else if ($value == 'alipay-facetoface') {
		$pay_banks['alipay-facetoface'] = 'alipay-facetoface';
	}else if ($value == 'wxpaynative') {
		$pay_banks['wxpaynative'] = 'wxpaynative';
	}else{
		$pay_banks[$value]  = $value;
	}
}


if (!$pay_banks[$type]) showmessage($lang['channelname'] . $lang['not_on'], 'home.php?mod=spacecp&ac=credit');

$needMoney = isset($rules_data[(string)$need]) ? (float)$rules_data[(string)$need] : 0.0;

if (!$config['money_on'] && $needMoney <= 0 && $need > 0) showmessage($lang['not_money_error'], 'home.php?mod=spacecp&ac=credit');

if ($needMoney <= 0) $needMoney = round(ceil(($need / $ec_ratio) * 100) / 100, 2);

if ($needMoney <= 0) showmessage('not money', 'home.php?mod=spacecp&ac=credit');

$orderid = date('Ymd') . uniqid();
$creatTime = time();
DB::query("INSERT INTO " . DB::table('forum_order') . " (orderid, status, uid, amount, price, submitdate) VALUES ('{$orderid}', '1', '{$_G['uid']}', '{$need}', '{$needMoney}', '{$creatTime}')");
if (!DB::affected_rows()) showmessage($lang['order_err']);

# $pay_url = zhifufmCore::getPayURL($needMoney, $orderid, $type, 'DZ:' . $_SERVER['HTTP_HOST'] . ':' . $_G['uid']);
# header('Location: ' . $pay_url);
# exit();
die(zhifufmCore::getPayForm($needMoney, $orderid, $type, 'DZ:' . $_G['uid']));
