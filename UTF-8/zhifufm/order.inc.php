<?php
if (!defined('IN_DISCUZ')) {
    exit('Access Denied');
}

include_once DISCUZ_ROOT.'./source/plugin/zhifufm/main.inc.php';

$pay_bank = unserialize($config['payType']);
$pay_banks = array();
foreach ($pay_bank as $key => $value) {
	if ($value == 'wechat') {
		$pay_banks['wechat'] = "wechat";
	} else if ($value == 'alipay') {
		$pay_banks['alipay'] = 'alipay';
	}else if ($value == 'alipaysign') {
		$pay_banks['alipaysign'] = 'alipaysign';
	}else if ($value == 'alipay-facetoface') {
		$pay_banks['alipay-facetoface'] = 'alipay-facetoface';
	}else if ($value == 'wxpaynative') {
		$pay_banks['wxpaynative'] = 'wxpaynative';
	}else{
		$pay_banks[$value]  = $value;
	}
}
if($_GET['mobile']){
    include template('zhifufm:mobile');
    exit();
}

?>
