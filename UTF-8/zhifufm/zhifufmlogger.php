<?php
if (!defined('IN_DISCUZ')) {
    exit('Access Denied');
}

$LEVELS = array(
    10 => 'DEBUG',
    20 => 'INFO',
    30 => 'WARN',
    40 => 'ERROR',
    50 => 'FATAL',
);

define('DEBUG', 10);
define('INFO', 20);
define('WARN', 30);
define('ERROR', 40);
define('FATAL', 50);

define('DAILY', 'daily');
define('MONTHLY', 'monthly');

function getTime()
{
    $mt = microtime(true);
    $micro = sprintf("%06d", ($mt - floor($mt)) * 1000000);
    $dt = new DateTime(date('Y-m-d H:i:s.' . $micro, $mt));
    return $dt->format('Y-m-d H:i:s.u');
}

class zhifufmLogger
{
    private $instance = NULL;

    function __construct($fileName, $level = INFO, $logrotate = DAILY)
    {
        $this->fileName = $fileName;
        $this->logLevel = $level;
        $this->logrotate = $logrotate;

        $lockFileName = $fileName . '.lock';
        $this->lockFile = fopen($lockFileName, 'w') or exit("Can't create $lockFileName!");
    }

    function __destruct()
    {
        if ($this->lockFile) {
            fclose($this->lockFile);
        }
    }

    private function _logFileName()
    {
        switch ($this->logrotate) {
            case MONTHLY:
                return $this->fileName . '.' . date('Ym');
            case DAILY:
            default:
                return $this->fileName . '.' . date('Ymd');
        }
    }

    private function _log($level, $message)
    {
        global $LEVELS;

        // assert(array_key_exists($level, $LEVELS), 'logging: level range ' . $level);
        if ($level < $this->logLevel) {
            return;
        }

        $fileName = $this->_logFileName();
        $fp = fopen($fileName, 'a+') or exit("Can't create $fileName!");

        $sessionID = session_id();
        $message = getTime() . ' [' . $LEVELS[$level] . '] [' . ($sessionID ? $sessionID : '-') . '] ' . $message . "\n";

        fwrite($fp, $message);
        fflush($fp);
        fclose($fp);
    }

    function log($level, $message)
    {
        flock($this->lockFile, LOCK_EX);
        $this->_log($level, $message);
        flock($this->lockFile, LOCK_UN);
    }

    function debug($message)
    {
        $this->log(DEBUG, $message);
    }

    function info($message)
    {
        $this->log(INFO, $message);
    }

    function warn($message)
    {
        $this->log(WARN, $message);
    }

    function error($message)
    {
        $this->log(ERROR, $message);
    }

    function fatal($message)
    {
        $this->log(FATAL, $message);
    }
}
