<?php
if (!defined('IN_DISCUZ')) {
    exit('Access Denied');
}

include_once DISCUZ_ROOT . './source/plugin/zhifufm/main.inc.php';
include_once DISCUZ_ROOT . './source/plugin/zhifufm/zhifufmcore.php';

$zhifufm = new zhifufmCore();
$msg = $zhifufm->apiNotify();
$result = array('sign' => $msg === 'success', 'msg' => $msg, 'ok' => false);

if ($result["sign"]) {
	
    $orderid = daddslashes($_GET['orderNo']);
    $zhifufm_order_id = daddslashes($_GET['orderNo']);
    $inTime = $_G['timestamp'];
    $submitdate = $_G['timestamp'] - 60 * 86400;
    $sql = "SELECT * FROM " . DB::table('forum_order') . " WHERE orderid='{$orderid}'";
    $logger->debug($sql);
    $order = DB::fetch_first($sql);
    if ($order) {
        $result["ok"] = 1;
        if ($order['status'] == 1) {
            # C::t('forum_order')->update($orderid, array('status' => '2', 'buyer' => "{$zhifufm_order_id} from plugin:zhifufm", 'confirmdate' => $_G['timestamp']));
            $sql = "UPDATE " . DB::table('forum_order') . " SET status='2', buyer='{$zhifufm_order_id} from plugin:zhifufm', confirmdate='{$inTime}' WHERE orderid='{$orderid}' and status='1'";
            $logger->debug($sql);
            $rs = DB::query($sql);
            if (!DB::affected_rows()) {
                $result["msg"] = 'data error';
                $result["ok"] = false;
            } else {
                updatemembercount($order['uid'], array($config['cextcredit'] => $order['amount']), 1, 'AFD', $order['uid']);
                $action = null; // 这里的 action 不知道该是什么，参考：upload/api/trade/notify_credit.php
                updatecreditbyaction($action, $uid = 0, $extrasql = array(), $needle = '', $coef = 1, $update = 1, $fid = 0);
                # C::t('forum_order')->delete_by_submitdate($_G['timestamp'] - 60 * 86400);
                $logger->debug('delete orders which is too old');
                DB::query("DELETE FROM " . DB::table('forum_order') . " WHERE submitdate<{$submitdate}");
                $logger->debug('create a notification to user#' . $order['uid']);
                notification_add($order['uid'], 'credit', 'addfunds', array(
                    'orderid' => $order['orderid'],
                    'price' => $order['price'],
                    'value' => $_G['setting']['extcredits'][$config['cextcredit']]['title'] . ' ' . $order['amount'] . ' ' . $_G['setting']['extcredits'][$config['cextcredit']]['unit']
                ), 1);
            }
        } else {
            $result["msg"] = 'success';
        }
    } else {
        $result["ok"] = false;
        $result["msg"] = 'no this order';
    }
    $logger->info(var_export($result, true));
}
if (!empty($_GET)) exit($result["msg"]);
if ($result["success"]) {
    dheader('location: /forum.php?mod=misc&action=paysucceed');
} else {
    showmessage($lang['order_handle_error'] . $result["msg"], '/home.php?mod=spacecp&ac=credit');
}
