<?php
if (!defined('IN_DISCUZ')) {
    exit('Access Denied');
}
global $_G;
include_once DISCUZ_ROOT . './source/plugin/zhifufm/util.func.php';

$lang = lang('plugin/zhifufm');

$config = _config();

if (empty($config['cextcredit'])) showmessage($lang['credits_addfunds'] . $lang['no_set'], 'home.php?mod=spacecp&ac=credit');
if (empty($config['merchantNum']) || empty($config['api_key'])) showmessage($lang['no_id'], '/admin.php?frames=yes&action=plugins');

$ec_ratio = $config['zhifufm_ratio'];


$rules_data = getRules($config['rule'], $ec_ratio);

require_once DISCUZ_ROOT . './source/plugin/zhifufm/zhifufmorder.php';
zhifufmOrder::apiConfig($config['merchantNum'], $config['api_key']);
