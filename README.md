# 支付FM插件（for DiscuzX）

#### 介绍
支付FM插件（for DiscuzX）
支持支付方式【后续会不断更新】   ：
 + 签约：支付宝当面付，支付宝网站支付，微信支付native通道；
 + 免签：支付宝个人收款码，微信个人收款码 

GBK版本体验网站：http://dzgbk.src168.com
UTF8版本体验网站：http://dzutf8.src168.com
订单回调情况可以通过形如 http://dzutf8.src168.com/plugin.php?id=zhifufm:action&do=viewdb 访问查看，dz.nnt.ltd替换为您的域名

详细安装说明：https://docs.nephalem.cn/read/zhifufm/dzplugin
插件升级：新版本直接覆盖旧版本，discuz后台点击插件的更新按钮

#### 软件架构
软件架构说明
插件自带数据库使用 SQLite3用于记录订单创建和接口通信情况，如不需要查看数据记录，可以修改action.inc.php  
` $invalidActions = array('check', 'notify', 'return', 'payr', 'result', 'viewdb');`  
修改为  
` $invalidActions = array('check', 'notify', 'return', 'payr', 'result');`  

#### 安装教程
详细教程：https://zhifu.fm/news/gonggao/20200924/169.html

1.  源码打包为zhifufm.zip,上传到Web根目录/source/plugin/   
2.  解压，后台点击安装
3.  配置您的支付FM平台的商户信息

#### 使用说明

1. 插件安装后可以设置购买各种积分
2. 详细说明：https://docs.nephalem.cn/read/zhifufm/dzplugin

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request

